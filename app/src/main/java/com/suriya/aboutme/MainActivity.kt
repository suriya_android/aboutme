package com.suriya.aboutme

import android.content.Context
import android.database.DatabaseUtils
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.suriya.aboutme.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding:ActivityMainBinding
    private val myName:Myname = Myname("Suriya")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        binding.apply {
            doneButton.setOnClickListener {v ->
                addNickName(v)
            }
            nicknameText.setOnClickListener {
                updatedNickName(it)
            }
        }
        binding.myName = myName
    }
    private fun addNickName(V:View){
        binding.apply {
            doneButton.visibility = View.GONE
            nicknameEdit.visibility = View.GONE
            myName?.nickname = nicknameEdit.text.toString()
            nicknameText.visibility = View.VISIBLE
            invalidateAll()
        }
        val inpMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inpMethodManager.hideSoftInputFromWindow(V.windowToken,0)
    }

    private fun updatedNickName(V:View){
        binding.apply {
            doneButton.visibility = View.VISIBLE
            nicknameEdit.visibility = View.VISIBLE
            V.visibility = View.GONE
            nicknameEdit.requestFocus()
            val inpMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inpMethodManager.showSoftInput(nicknameEdit,0)
        }

    }
}